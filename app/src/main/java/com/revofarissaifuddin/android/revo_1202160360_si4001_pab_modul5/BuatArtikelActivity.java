package com.revofarissaifuddin.android.revo_1202160360_si4001_pab_modul5;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class BuatArtikelActivity extends AppCompatActivity {

    private CoordinatorLayout coord_buat;
    private EditText edJudul, edAuthor, edTanggal, edDesc;
    private ArtikelDataSource artikelDB;
    boolean theme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean theme = preferences.getBoolean("dark_theme", false);
        boolean font = preferences.getBoolean("font_large", false);
        if (theme && font) {
            setTheme(R.style.AppTheme_Dark_FontLarge);
        } else if (theme) {
            setTheme(R.style.AppTheme_Dark_FontNormal);
        } else if (font) {
            setTheme(R.style.AppTheme_FontLarge);
        }
        setContentView(R.layout.activity_buat_artikel);
        initUI();
//todo: Add DatePicker Intent, getCurrentDate for Create and Edit
    }

    private void initUI() {
        edJudul = findViewById(R.id.buat_edittext_judul);
        edAuthor = findViewById(R.id.buat_edittext_author);
        edTanggal = findViewById(R.id.buat_edittext_tanggal);
        edDesc = findViewById(R.id.buat_edittext_desc);
        coord_buat = findViewById(R.id.buat_coordinator);
        artikelDB = new ArtikelDataSource(this);
        artikelDB.open();
    }

    public void buatArtikel(View view) throws InterruptedException {
        String judul, author, tanggal, desc;
        Artikel artikel = null;
        if (TextUtils.isEmpty(edJudul.getText())) {
            Snackbar.make(coord_buat, "Judul wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edJudul.setError("Judul wajib diisi!");
            return;
        }
        if (TextUtils.isEmpty(edAuthor.getText())) {
            Snackbar.make(coord_buat, "Nama penulis wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edAuthor.setError("Nama penulis wajib diisi!");
            return;
        }
        if (TextUtils.isEmpty(edTanggal.getText())) {
            Snackbar.make(coord_buat, "Tanggal tidak benar!", Snackbar.LENGTH_LONG)
                    .show();
            edTanggal.setError("Tanggal tidak benar!");
            return;
        }
        if (TextUtils.isEmpty(edDesc.getText())) {
            Snackbar.make(coord_buat, "Deskripsi wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edJudul.setError("Deskripsi wajib diisi!");
            return;
        }
        judul = edJudul.getText().toString();
        author = edAuthor.getText().toString();
        tanggal = edTanggal.getText().toString();
        desc = edDesc.getText().toString();

        artikel = artikelDB.createArtikel(judul, author, desc, tanggal);
        Snackbar.make(coord_buat, "Data terinput! \n"
                +artikel.getTitle(), Snackbar.LENGTH_LONG).show();
        finish();


    }
}