package com.revofarissaifuddin.android.revo_1202160360_si4001_pab_modul5;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    private TextView tvJudul, tvAuthor, tvDate, tvDesc;
    private ImageButton btnShare, btnEdit, btnDelete;
    private String judul, author, date, desc;
    Bundle i;
    boolean theme;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean theme = preferences.getBoolean("dark_theme", false);
        boolean font = preferences.getBoolean("font_large", false);
        if (theme && font) {
            setTheme(R.style.AppTheme_Dark_FontLarge);
        } else if (theme) {
            setTheme(R.style.AppTheme_Dark_FontNormal);
        } else if (font) {
            setTheme(R.style.AppTheme_FontLarge);
        }
        setContentView(R.layout.activity_details);
        initUI();
    }

    private void initUI() {
        tvJudul = findViewById(R.id.details_txv_namajudul);
        tvAuthor = findViewById(R.id.details_txv_author_name);
        tvDate = findViewById(R.id.details_txv_createddate);
        tvDesc = findViewById(R.id.details_txv_content);
        btnEdit = findViewById(R.id.details_btn_edit);
        btnDelete = findViewById(R.id.details_btn_delete);
        btnShare = findViewById(R.id.details_btn_share);

        if (getIntent().getExtras() != null) {
            i = getIntent().getExtras();
            id = i.getLong("id");
            judul = i.getString("judul");
            tvJudul.setText(judul);
            author = i.getString("author");
            tvAuthor.setText(author);
            date = i.getString("date");
            tvDate.setText(date);
            desc = i.getString("desc");
            tvDesc.setText(desc);
        }

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EditArtikel.class);
                Bundle bundle = new Bundle();
                bundle.putLong("id", id);
                bundle.putString("judul", judul);
                bundle.putString("author", author);
                bundle.putString("date", date);
                bundle.putString("desc", desc);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBodyText = judul
                        + " oleh " + author +
                        "\n" + desc
                        + "\n\n. Dibagikan oleh aplikasi Voltpad (bukan Wattpad).";
                intent.putExtra(Intent.EXTRA_SUBJECT, judul);
                intent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                v.getContext().startActivity(Intent.createChooser(intent, "Choose sharing method"));
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(DetailsActivity.this);
                alert.setTitle("Hapus")
                        .setMessage("Apakah Anda yakin untuk menghapus?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ArtikelDataSource artikelDB = new ArtikelDataSource(DetailsActivity.this);
                                artikelDB.open();
                                artikelDB.deleteArtikelById(id);
                                dialog.dismiss();
                                finish();
                                startActivity(new Intent(DetailsActivity.this, ViewArtikel.class));
                            }
                        }).setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });
    }
}