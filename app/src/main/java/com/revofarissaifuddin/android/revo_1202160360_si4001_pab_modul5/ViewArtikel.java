package com.revofarissaifuddin.android.revo_1202160360_si4001_pab_modul5;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.Toolbar;

import java.util.ArrayList;

public class ViewArtikel extends AppCompatActivity {
    private ArtikelDataSource mDataSource = new ArtikelDataSource(this);
    RecyclerView mRecycler;
    Toolbar toolbar;
    private ArrayList<Artikel> values;
    private ArtikelAdapter mAdapter;
    boolean theme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean theme = preferences.getBoolean("dark_theme", false);
        boolean font = preferences.getBoolean("font_large", false);
        if (theme && font) {
            setTheme(R.style.AppTheme_Dark_FontLarge);
        } else if (theme) {
            setTheme(R.style.AppTheme_Dark_FontNormal);
        } else if (font) {
            setTheme(R.style.AppTheme_FontLarge);
        }
        setContentView(R.layout.activity_view_artikel);
        mRecycler = findViewById(R.id.view_recycler);
        mDataSource.open();
        values = mDataSource.readArtikel();
        mAdapter = new ArtikelAdapter(this, values);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        mRecycler.setAdapter(mAdapter);
        ItemTouchHelper.Callback callback = new SwipeToDeleteCallback(mAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(mRecycler);
    }
}

